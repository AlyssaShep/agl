package poste;

public class Courriel {
	public String destination;
	public String titre;
	public String message;
	public String[] piecesJointes;
	
	public Courriel(String destination, String titre, String message, String[] pieces){
		this.destination = destination;
		this.titre = titre;
		this.message = message;
		this.piecesJointes = pieces;
	}
	
	public void setDestination(String destination) {
		this.destination = destination;
	}
	
	public void setTitre(String titre) {
		this.titre = titre;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public void setPiecesJointes(String[] piecesJointes) {
		this.piecesJointes = piecesJointes;
	}
	
	public void setPiecesJointesIndex(String piece, int i) {
		this.piecesJointes[i] = piece;
	}
	
	public String getDestination() {
		return destination;
	}
	
	public String getTitre() {
		return titre;
	}
	
	public String getMessage() {
		return message;
	}
	
	public String[] getPiecesJointes() {
		return piecesJointes;
	}
	
	public String getPieceJointeIndex(int i) {
		return piecesJointes[i];
	}
	
	// verifications
	
	public boolean verificationDestionation() {
		String pattern = "^(.+)@(.+)$";
		
		boolean verif = destination.matches(pattern);
		return verif;
	}
	
	public boolean aTitre() {
		if(titre == null || titre == "" || titre == " ") {
			return false;
		}
		return true;
	}
	
	public boolean messagePrecisionPiecesJointes() {
		String[] motPJ = {"PJ","joint","jointe","jointes", "joints", "pièce", "pièces"};
		String[] casse = message.split(" ");
		int taille = casse.length;

		for(int i = 0; i < taille; i++) {	
			for(int j = 0; j < 7; j++) {
				if(casse[i].equals(motPJ[j])) {
					return true;
				}
			}
		}
		
		return false;	
	}
	
	public boolean aBienJoint() {
		boolean scanMessage = messagePrecisionPiecesJointes();
		
		if(scanMessage && piecesJointes == null) {
			return false;
		}
		
		return true;
	}
	
	

}
