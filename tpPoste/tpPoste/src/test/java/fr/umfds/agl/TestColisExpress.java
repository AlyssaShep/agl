package fr.umfds.agl;

import poste.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.AfterAll;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.params.ParameterizedTest;
//import org.junit.jupiter.params.provider.Arguments;
//import org.junit.jupiter.params.provider.MethodSource;
//import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

public class TestColisExpress {
	
	ColisExpress ce1;
	ColisExpress ce2;
	ColisExpress ce3;
	ColisExpress ce4;
	
	String origine3 = "France";
	String origine4 = "Pologne";
	
	String destination3 = "Mayotte";
	String destination4 = "Martinique";
	
	String codePostal3 = "34444";
	String codePostal4 = "333333";
	
	float poids3 = 60f;
	float poids4 = 2f;
	
	float volume3 = 12f;
	float volume4 = 1f;
	
	Recommandation r3 = Recommandation.deux;
	Recommandation r4 = Recommandation.un;
	
	String declareContenu3 = "un Benoit";
	String declareContinu4 = "un micro";
	
	float valeurDeclaree3 = 5.3f;
	float valeurDeclaree4 = 5.2f;
	
	@BeforeEach
	public void setUp() throws ColisExpressInvalide {

		ce1 = new ColisExpress(origine3, destination4, codePostal3, poids4, volume3, Recommandation.zero, declareContenu3, valeurDeclaree4, true);
		ce4 = new ColisExpress(origine4, destination4, codePostal4, poids4, volume4, r4, declareContinu4, valeurDeclaree4, false);
	}
	
	@Test void creationInvalide() {
		assertThrows(ColisExpressInvalide.class, () -> {
			ce2 = new ColisExpress(origine3, destination3, codePostal3, poids3, volume3, r3, declareContenu3, valeurDeclaree3, true);
			ce3 = new ColisExpress(origine3, destination3, codePostal3, poids3, volume3, r3, declareContenu3, valeurDeclaree3, true);
		});
	}
	
	@Test
	public void testTarifAffranchissementValide() {
		assertEquals(33,ce1.tarifAffranchissement());
		assertEquals(30,ce4.tarifAffranchissement());
	}
	
	@Test
	public void testTarifAffranchissementInvalide() {
		assertThrows(ColisExpressInvalide.class, () -> {
			ce2 = new ColisExpress(origine3, destination3, codePostal3, poids3, volume3, r3, declareContenu3, valeurDeclaree3, true);
			ce3 = new ColisExpress(origine3, destination3, codePostal3, poids3, volume3, r3, declareContenu3, valeurDeclaree3, true);

			ce2.tarifAffranchissement();
			ce3.tarifAffranchissement();
		});
	}
	
	@Test
	public void testTypeObjetPostalValide() {
		assertEquals("Colis express",ce1.typeObjetPostal());
		assertEquals("Colis express",ce4.typeObjetPostal());
	}
	
	@Test
	public void testTypeObjetPostalInvalide() {
		assertThrows(ColisExpressInvalide.class, () -> {
			ce2 = new ColisExpress(origine3, destination3, codePostal3, poids3, volume3, r3, declareContenu3, valeurDeclaree3, true);
			ce3 = new ColisExpress(origine3, destination3, codePostal3, poids3, volume3, r3, declareContenu3, valeurDeclaree3, true);

			ce2.tarifAffranchissement();
			ce3.tarifAffranchissement();
		});
	}
	
	@Test
	public void testToStringValide() {
		assertEquals("Colis express 34444/Martinique/0/12.0/5.2/2.0",ce1.toString());
		assertEquals("Colis express 333333/Martinique/1/1.0/5.2/2.0",ce4.toString());
	}
	
	@Test
	public void testToStringInvalide() {
		assertThrows(ColisExpressInvalide.class, () -> {
			ce2 = new ColisExpress(origine3, destination3, codePostal3, poids3, volume3, r3, declareContenu3, valeurDeclaree3, true);
			ce3 = new ColisExpress(origine3, destination3, codePostal3, poids3, volume3, r3, declareContenu3, valeurDeclaree3, true);

			ce3.toString();
			ce2.toString();
		});
	}

}
