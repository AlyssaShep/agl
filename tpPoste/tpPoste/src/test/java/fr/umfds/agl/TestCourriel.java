package fr.umfds.agl;
import poste.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.AfterAll;
//import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.util.stream.Stream;

/**
 * Unit test for simple App.
 */
public class TestCourriel 
{
//	test 
	Courriel c1;
	Courriel c2;
	Courriel c3;
	Courriel c4;
	
	@BeforeEach
	public void setUp() {
//		public Courriel(String destination, String titre, String message, String[] pieces)
		String[] pieces = {"p1","p2"};
		c1 = new Courriel("elkfekf","jesaispas", "message kef", pieces);
		c2 = new Courriel("ma@mef.fr","jesaispas", "eifh message", pieces);
		c3 = new Courriel("@fehoi.fr","", "message pièce", pieces);
		c4 = new Courriel("@iefh"," ", "message PJ", pieces);
	}
	
	@Test
	public void testmessagePrecisionPiecesJointes() {
		assertEquals(false,c1.messagePrecisionPiecesJointes());
		assertEquals(false,c2.messagePrecisionPiecesJointes());
		assertEquals(true,c3.messagePrecisionPiecesJointes());
		assertEquals(true,c4.messagePrecisionPiecesJointes());
	}
	
	@Test
	public void testGetPiecesJointes() {
		String[] pieces = {"p1","p2"};
		assertArrayEquals(pieces, c1.getPiecesJointes());
		assertArrayEquals(pieces, c2.getPiecesJointes());
		assertArrayEquals(pieces, c3.getPiecesJointes());
		assertArrayEquals(pieces, c4.getPiecesJointes());
	}
	
	@Test
	public void testGetMessage() {
		assertEquals("message kef", c1.getMessage());
		assertEquals("eifh message", c2.getMessage());
		assertEquals("message pièce", c3.getMessage());
		assertEquals("message PJ", c4.getMessage());
	}
	
	@Test
	public void testGetTitre() {
		assertEquals("jesaispas", c1.getTitre());
		assertEquals("jesaispas", c2.getTitre());
		assertEquals("", c3.getTitre());
		assertEquals(" ", c4.getTitre());
	}
	
	@Test
	public void testGetDestination() {
		assertEquals("elkfekf", c1.getDestination());
		assertEquals("ma@mef.fr", c2.getDestination());
		assertEquals("@fehoi.fr", c3.getDestination());
		assertEquals("@iefh", c4.getDestination());
	}
	
	@ParameterizedTest
	@ValueSource(strings = {"adresses", "jspme.fr","@ihfep.fr"})
	void testEmailParametreFalseEmail(String adress) {
		String[] pieces = {"p1","p2"};
		Courriel c = new Courriel("elkfekf","jesaispas", "message", pieces);
		c.setDestination(adress);
		assertEquals(false,c1.verificationDestionation());
	}
	
	
//	test parametre avec display personnalise
	@ParameterizedTest(name = "{index} - {0} should return {1}")
	@MethodSource("destinationTest")
	void destinTest(String adress, boolean expected, Courriel c1) {
		c1.setDestination(adress);
		assertEquals(expected, c1.verificationDestionation());
	}
	
	private static Stream<Arguments> destinationTest(){
		String[] pieces = {"p1","p2"};
		Courriel c = new Courriel("elkfekf","jesaispas", "message", pieces);
		return Stream.of(
				Arguments.of("adres@ses.ihefih", true,c),
				Arguments.of("adresses.fr", false,c),
				Arguments.of("adresses", false,c),
				Arguments.of("@me.fr", false,c),
				Arguments.of("fr@ihfep.fr", true,c),
				Arguments.of("uefh@iepf.fr", true,c)
				);
	}
	
	@ParameterizedTest
	@ValueSource(strings = {"adresses", "jspme.fr","@ihfep.fr"})
	void testSetPiecesJointesIndex(String adress) {
		String[] pieces = {"p1","p2"};
		Courriel c = new Courriel("elkfekf","jesaispas", "message", pieces);
		c.setPiecesJointesIndex(adress,0);
		assertEquals(adress,c.getPieceJointeIndex(0));
	}
	
	@Test
	public void testABienJoint() {
		assertEquals(true,c1.aBienJoint());
		assertEquals(true,c2.aBienJoint());
		assertEquals(true,c3.aBienJoint());
		c4.setPiecesJointes(null);
		assertEquals(false,c4.aBienJoint());
	}
	
	@ParameterizedTest
	@ValueSource(strings = {"adresses", "jspme.fr","@ihfep.fr"})
	void testSetTitre(String titre) {
		String[] pieces = {"p1","p2"};
		Courriel c = new Courriel("elkfekf","jesaispas", "message", pieces);
		c.setTitre(titre);
		assertEquals(titre,c.getTitre());
	}
	
	@ParameterizedTest
	@ValueSource(strings = {"adresses", "jspme.fr","@ihfep.fr"})
	void testSetMessage(String message) {
		String[] pieces = {"p1","p2"};
		Courriel c = new Courriel("elkfekf","jesaispas", "message", pieces);
		c.setMessage(message);
		assertEquals(message,c.getMessage());
	}
	
	@Test
	public void testATitre() {
		String[] pieces = {"p1","p2"};
		Courriel c = new Courriel("elkfekf","jesaispas", "message", pieces);
		c.setTitre(null);
		assertEquals(false,c.aTitre());
		c.setTitre("");
		assertEquals(false,c.aTitre());
		c.setTitre(" ");
		assertEquals(false,c.aTitre());
		c.setTitre("fehfioeh");
		assertEquals(true,c.aTitre());
	}
	
}
