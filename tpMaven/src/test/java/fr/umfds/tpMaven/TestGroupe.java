package fr.umfds.tpMaven;

import gestionTER.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.AfterAll;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.params.ParameterizedTest;
//import org.junit.jupiter.params.provider.Arguments;
//import org.junit.jupiter.params.provider.MethodSource;
//import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

//import java.util.stream.Stream;

public class TestGroupe {
	
	Groupe g1;
	Groupe g2;
	Groupe g3;
	Groupe g4;
	
	String[] nom = {"baba","truc","encore","jsp"};
	String[] nom2 = {"b","truc","encore","jsp","jhwf"};
	String[] nom3 = {"a","truc","encore","jsp","jhwf","enfieh"};
	String[] nom4 = {"b","truc","encore","jsp","jhwf","enfieh","foieh"};
	
	@BeforeEach
	public void setUp() {	
		
		g1 = new Groupe(nom);
		g2 = new Groupe(nom2);
		g3 = new Groupe(nom3);
		g4 = new Groupe(nom4);
	}
	
	@Test
	public void testGetNbEleves() {
		assertEquals(4,g1.getNbEleves());
		assertEquals(5,g2.getNbEleves());
		assertEquals(6,g3.getNbEleves());
		assertEquals(7,g4.getNbEleves());
	}
	
	@Test
	public void testgetId() {
		assertEquals(4,g1.getId());
		assertEquals(5,g2.getId());
		assertEquals(6,g3.getId());
		assertEquals(7,g4.getId());
	}

}
