package hongrois;
import java.util.*;

public class Hongrois implements HongroisI {
	private int hauteur;
	private int largeur;
	private List<List<Integer>> adjList;
	
//	constructors
	public Hongrois(int hauteur, int largeur) {
		this.hauteur = hauteur;
		this.largeur = largeur;
	}
	
	public Hongrois(int hauteur, int largeur, List<List<Integer>> adjList) {
		this.hauteur = hauteur;
		this.largeur = largeur;
		this.adjList = adjList;
	}
	
//	setter
	
	public void setHauteur(int hauteur) {
		this.hauteur = hauteur;
	}
	
	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}
	
	public void setAdjacenceList(List<List<Integer>> adjList) {
		this.adjList = adjList;
	}

	
//	getter
	
	public int getHauteur() {
		return this.hauteur;
	}
	
	public int getLargeur() {
		return this.largeur;
	}
	
	public List<List<Integer>> getListAdj(){
		return this.adjList;
	}
	
//	functions
	/**
	* Calcule et retourne les affectations
	* @param phaseNumber : 1 pour une affectation partielle, 2 pour une
	affectation globale
	* @return une liste de couples (i, j) qui correspondent aux affectations
	calculées
	*/
	public List<List<Integer>> affectation(int phase){
		return adjList;
	}
}
