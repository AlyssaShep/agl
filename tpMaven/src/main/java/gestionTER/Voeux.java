package gestionTER;
import java.util.*;

public class Voeux {
    private Sujet sujet;
    private int preference;
    
    //constructor
    
    public Voeux(Sujet sujet, int preference) {
        this.sujet = sujet;
        this.preference = preference;
    }
    
    public Voeux() {
    	
    }
    
    //setter
    
    public void setSujet(Sujet sujet) {
        this.sujet = sujet;
    }
    
    public void setPreference(int preference) {
        this.preference = preference;
    }
    
    //getter
    
    public Sujet getSujet() {
        return this.sujet;
    }
    
    public int getPreference() {
        return this.preference; 
    }
    
//    functions
    
}