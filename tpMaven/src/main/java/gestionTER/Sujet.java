package gestionTER;

import java.util.concurrent.atomic.AtomicInteger;

public class Sujet {
    private static final AtomicInteger ID_FACTORY = new AtomicInteger();
    private final int id;
    private String titre;
    private String resume;
    private String nomEnseignant;
    
    //constructor
    
    public Sujet() {
    	this.id = ID_FACTORY.getAndIncrement();
    }
    
    public Sujet(String titre, String resume, String nomEnseignant) {
        this.id = ID_FACTORY.getAndIncrement();
        this.titre = titre;
        this.resume = resume;
        this.nomEnseignant = nomEnseignant;
    }
    
    //setter
    
    public void setTitre(String titre) {
        this.titre = titre;
    }
    
    public void setResume(String resume) {
    	this.resume = resume;
    }
    
    public void setNomEnseignant(String nomEnseignant) {
    	this.nomEnseignant = nomEnseignant;
    }
    
    //getter
    
    public int getId() {
        return this.id;
    }
    
    public String getTitre() {
        return this.titre;
    }
    
    public String getResume() {
    	return this.resume;
    }
    
    public String getNomEnseignant() {
    	return this.nomEnseignant;
    }
    
    //functions
}
