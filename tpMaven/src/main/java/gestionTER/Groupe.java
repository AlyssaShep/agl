package gestionTER;
import java.util.*;
import java.util.concurrent.atomic.*;

public class Groupe {
    
    private static final AtomicInteger ID_FACTORY = new AtomicInteger();
    private final int id;
    private String[] nom;
    private int affectation = -1;
    //all groups got 1 to 5 wishes to make
    private ArrayList<Voeux> ordre = new ArrayList<Voeux>(5);
    
    //constructor
    
    public Groupe(String[] nom) {
        this.id = ID_FACTORY.getAndIncrement();
        this.nom = nom;
    }
    
    //setter
    
    public void setNom(String[] nom) {
        this.nom = nom;
    }
    
    public void setNomIndex(String nom, int i) {
        this.nom[i] = nom;
    }
    
    public void setVoeu(Voeux voeux) {
        this.ordre.add(voeux);
    }
    
    public void setVoeuIndex(Voeux voeux, int i) {
        this.ordre.add(i, voeux);
    }
    
    public void setAffectation(int idSujet) {
        this.affectation = idSujet;
    }
    
    //getter
    
    public int getNbEleves() {
        return nom.length;
    }
    
    public int getId() {
        return this.id;
    }
    
    public int getAffectation() {
    	return this.affectation;
    }
    
    public String[] getNom() {
        return this.nom;
    }
    
    public String getNomIndex(int i) {
        return this.nom[i];
    }
    
    public Voeux getIndexVoeux(int i) {
        return this.ordre.get(i);
    }
    
    public ArrayList<Voeux> getVoeux(){
    	return this.ordre;
    }
    
    //functions
    
    public boolean estDansVoeux(int idSujet) {
    	ArrayList<Voeux> voeux = this.getVoeux();
    	int nbVoeux = voeux.size();
    	for(int i = 0; i < nbVoeux; i++) {
    		if(voeux.get(i).getSujet().getId() == idSujet) {
    			return true;
    		}
    	}
    	return false;
    }
    
    public int IndexDansVoeux(int idSujet) {
    	ArrayList<Voeux> voeux = this.getVoeux();
    	int nbVoeux = voeux.size();
    	for(int i = 0; i < nbVoeux; i++) {
    		if(voeux.get(i).getSujet().getId() == idSujet) {
    			return i;
    		}
    	}
    	return -1;
    }
}
