package gestionTER;
import java.util.*;
import java.io.*;
import java.nio.file.Paths;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;


public class GestionTER {

	public static boolean isFree(List<Groupe> listGroupes, int idSujet)  {
    	
    	int taille = listGroupes.size();
    	for(int i = 0; i < taille; i++) {
    		if(listGroupes.get(i).getAffectation() == idSujet) {
    			return true;
    		}
    	}
    	
    	return false;
    }
	
	public static void writingGroupes(int nbrG, Groupe[] a) throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper ecritureG = new ObjectMapper();
		File output = new File("src/main/outputs/Groupes.json");
//		ecritureG.writeValue(output, nbrG);
		ecritureG.writeValue(output, a);
	}
	
	public static void writingGroupesList(int nbrG, List<Groupe> a) throws JsonGenerationException, JsonMappingException, IOException{
		ObjectMapper ecritureG = new ObjectMapper();
		File output = new File("src/main/outputs/Groupes.json");
		ecritureG.writeValue(output, nbrG);
		ecritureG.writeValue(output, a);
	}
	
	public static void writingSubjets(int nbSujet, Sujet[] s) throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper ecritureS = new ObjectMapper();
		File outputS = new File("src/main/outputs/Sujets.json");
		ecritureS.writeValue(outputS, nbSujet);
		ecritureS.writeValue(outputS, s);
	}
	
	public static void writingSubjetsList(int nbSujet, List<Sujet> s) throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper ecritureS = new ObjectMapper();
		File outputS = new File("src/main/outputs/Sujets.json");
		ecritureS.writeValue(outputS, nbSujet);
		ecritureS.writeValue(outputS, s);
	}
	
	public static void creationGestion() throws JsonGenerationException, JsonMappingException, IOException {
		Scanner sc = new Scanner(System.in);
		
		try {
			
			System.out.println("Bienvenue dans la gestion de votre TER. \n");
			System.out.println("Tout d'abord, donnez le nombre de groupes que vous avez. (Attention le nombre d'élèves par groupe est de 5 maximum): ");	
			//checking if the number of groups is positive
			int nbrG = sc.nextInt();
			while(nbrG < 0) {
				System.out.println("La saisie de votre nombre de groupe est incorrect. Veuillez recommencer: ");
				nbrG = sc.nextInt();
			}
			
			//checking if the number is correct for the user
			System.out.println(" \nVous avez écrit : \n");
			System.out.println(nbrG);
			System.out.println("\nEtes-vous sûr ? (y/n) : \n");
			String r = sc.next();
			char rep = r.charAt(0);
			//verification que la confirmation est la bonne
			while(rep != 'y' && rep != 'n') {
				System.out.println("\nEtes-vous sûr ? (y/n) : \n");
				r = sc.next();
				rep = r.charAt(0);
			}
			
			while(rep == 'n') {
				System.out.println("\nCombien de groupes avez-vous ?");
				nbrG = sc.nextInt();
				
				while(nbrG < 0) {
					System.out.println("La saisie de votre nombre de groupe est incorrect. Veuillez recommencer: ");
					nbrG = sc.nextInt();
				}	
				
				System.out.println("\nEtes-vous sûr ? (y/n) : \n");
				r = sc.nextLine();
				rep = r.charAt(0);
				
				while(rep != 'y' && rep != 'n') {
					System.out.println("\nEtes-vous sûr ? (y/n) : \n");
					r = sc.next();
					rep = r.charAt(0);
				}
			}
			
			//Creation des groupes
			Groupe[] a = new Groupe[nbrG];
			
			for(int i = 0; i < nbrG; i++) {
				System.out.println("\nCombien d'élèves dans le groupe ?");
				int nbEleves = sc.nextInt();
				String[] listEleves = new String[nbEleves];
				
				System.out.println("\nDonner les noms des élèves : ");
				
				for(int j = 0; j < nbEleves; j++) {
					System.out.println("\nDonner l'élève "+(j+1)+" : \n");
					String eleve = sc.next();
					listEleves[j] = eleve;
				}
				
				a[i] = new Groupe(listEleves);
			}
			
//			ecritures dans le fichier json des infos
			writingGroupes(nbrG, a);
			
			System.out.println("\nRécap' des groupes : \n");
			for(int i = 0; i < nbrG; i++) {
				System.out.println("\nGroupe "+a[i].getId()+" : \n");
				System.out.println("\nNoms des élèves : \n");
				int taille = a[i].getNbEleves();
				for(int j = 0; j < taille; j++) {
					System.out.println(a[i].getNomIndex(j));
				}
				
			}
			
			
			//Creation des sujets
			System.out.println("\nCombien de sujet ?");
			int nbSujet = sc.nextInt();
			Sujet[] s = new Sujet[nbSujet];
			
			System.out.println(" \nVous avez écrit : ");
			System.out.println(nbSujet);
			sc.nextLine();
			
			
			for(int i = 0; i < nbSujet; i++) {
				System.out.println("\nLe titre ? ");
				String titre = sc.nextLine();
				
				System.out.println("Faites un résumé sur le sujet : ");
				String resume = sc.nextLine();
				
				System.out.println("Quel est l'enseignant responsable ?");
				String nomEnseignant = sc.nextLine();
				
				s[i] = new Sujet(titre, resume, nomEnseignant);
				
			}
			
// ecritures des sujets dans le fichier json
			writingSubjets(nbSujet, s);
			
			
			//Recap des sujets
			System.out.println("\nRécap' des sujets : \n");
			for(int i = 0; i < nbSujet; i++) {
				System.out.println("\nSujet "+s[i].getId()+" : \n");
				System.out.println(s[i].getTitre());
				System.out.println(s[i].getResume());
				System.out.println(s[i].getNomEnseignant());
			}
			
			
		} catch(InputMismatchException e) {
			System.out.println("Ce n'est pas un nombre");
		}
		
		sc.close();
	}

	public static void modificationSujet() throws JsonGenerationException, JsonMappingException, IOException {
		Scanner sc = new Scanner(System.in);
		System.out.println("Vous pouvez changer les sujets que vous avez créé. Voici les Sujets, séléctionnez celui que vous voulez changer : \n");
	
		ObjectMapper readSujets = new ObjectMapper();
		List<Sujet> listSujet = Arrays.asList(readSujets.readValue(Paths.get("src/main/outputs/Sujets.json").toFile(), Sujet[].class));
		
		int taille = listSujet.size();
		
		for(int i = 0; i < taille; i++) {
			System.out.println("("+listSujet.get(i).getId()+")"+" Titre : "+listSujet.get(i).getTitre());
			System.out.println("Resume : "+listSujet.get(i).getResume()+"\n");
			System.out.println("Enseignant responsable : "+listSujet.get(i).getNomEnseignant()+"\n");
		}
		System.out.println("("+taille+") sortir \n");
		
		int rep = sc.nextInt();
		
		int repMin = listSujet.get(0).getId();
		int repMax = listSujet.get(taille-1).getId();
		
		if(rep <= repMax && rep >= repMin) {
			System.out.println("Que voulez-vous changer ? :\n");
			System.out.println("(1) le titre ? \n");
			System.out.println("(2) le resume ? \n");
			System.out.println("(3) l'enseignant responsable ? \n");
			int repC = sc.nextInt();
			
			while(repC < 4 && repC > 0) {
				sc.nextLine();
				if(repC == 1) {
					System.out.println("Donner le nouveau titre : \n");
					String titre = sc.nextLine();
					listSujet.get(rep).setTitre(titre);
				}
				else if(repC == 2){
					System.out.println("Donner le nouveau resume : \n");
					String resume = sc.nextLine();
					listSujet.get(rep).setResume(resume);
				}
				else {
					System.out.println("Donner le nouvel enseignant responsable : \n");
					String enseignant = sc.nextLine();
					listSujet.get(rep).setNomEnseignant(enseignant);
				}
				
				System.out.println("Voulez-vous changer autre chose ? :\n");
				System.out.println("(1) le titre ? \n");
				System.out.println("(2) le resume ? \n");
				System.out.println("(3) l'enseignant responsable ? \n");
				repC = sc.nextInt();
			}
			
			writingSubjetsList(taille, listSujet);
		}
		
		sc.close();
		return;
	}
		
	public static void modificationGroupe() throws JsonGenerationException, JsonMappingException, IOException {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Vous pouvez changer les groupes que vous avez créé. Voici les groupes, séléctionnez celui que vous voulez changer : \n");
	
		ObjectMapper readGroupes = new ObjectMapper();
		List<Groupe> listGroupes = Arrays.asList(readGroupes.readValue(Paths.get("src/main/outputs/Groupes.json").toFile(), Groupe[].class));
		
		int taille = listGroupes.size();
		
		for(int i = 0; i < taille; i++) {
			System.out.println("("+listGroupes.get(i).getId()+")"+" Noms : "+listGroupes.get(i).getNom());
			int tailleV = listGroupes.get(i).getVoeux().size();
			for(int j = 0; j < tailleV; j++) {
				System.out.println("Voeux n*"+(j+1)+" titre : "+listGroupes.get(i).getIndexVoeux(j).getSujet().getTitre());
			}
		}
		System.out.println("("+taille+") sortir \n");
		
		int rep = sc.nextInt();
		
		int repMin = listGroupes.get(0).getId();
		int repMax = listGroupes.get(taille-1).getId();
		
		if(rep <= repMax && rep >= repMin) {
			System.out.println("Que voulez-vous changer ? :\n");
			System.out.println("(1) les noms ? \n");
			System.out.println("(2) les voeux ? \n");
			int repC = sc.nextInt();
			
			while(repC < 3 && repC > 0) {
				sc.nextLine();
				if(repC == 1) {
					System.out.println("Quel nom voulez-vous changer ? \n");
					int nbEleves = listGroupes.get(rep).getNbEleves();
					for(int i = 0; i < nbEleves; i++) {
						System.out.println("("+i+") "+listGroupes.get(rep).getNomIndex(i));
					}
					int choix = sc.nextInt();
					sc.nextLine();
					if(choix >= 0 && choix < nbEleves) {
						System.out.println("Donnez le nouveau nom : \n");
						String nom = sc.nextLine();
						listGroupes.get(rep).setNomIndex(nom, choix);
						
					} 
				}
				else {
					System.out.println("Quel voeu voulez-vous changer ? (Si le voeu est déjà présent dans la liste, il sera échangé avec celui à la place correspondante \n");
					int tailleVo = listGroupes.get(rep).getVoeux().size();
					for(int j = 0; j < tailleVo; j++) {
						System.out.println("Voeux n*"+j+" titre : "+listGroupes.get(rep).getIndexVoeux(j).getSujet().getTitre());
					}
					
					System.out.println("("+tailleVo+") sortir");
					int choix = sc.nextInt();
					
					if(choix >= 0 && choix < tailleVo) {
						System.out.println("Choisissez votre nouveau sujet : ");
						ObjectMapper readSujets = new ObjectMapper();
						List<Sujet> listSujet = Arrays.asList(readSujets.readValue(Paths.get("src/main/outputs/Sujets.json").toFile(), Sujet[].class));
						
						int tailleS = listSujet.size();
						for(int i = 0; i < tailleS; i++) {
							boolean libre = isFree(listGroupes,listSujet.get(i).getId());
							if(libre) {
								System.out.println("("+listSujet.get(i).getId()+")"+" Titre : "+listSujet.get(i).getTitre());
								System.out.println("Resume : "+listSujet.get(i).getResume()+"\n");
								System.out.println("Enseignant responsable : "+listSujet.get(i).getNomEnseignant()+"\n");
							}
						}
						
						int choixN = sc.nextInt();
						int idSujet = listSujet.get(choixN).getId();
						boolean estDansVoeux = listGroupes.get(rep).estDansVoeux(idSujet);
						if(estDansVoeux) {
							int idVoeux = listGroupes.get(rep).IndexDansVoeux(idSujet);
							Voeux temp = new Voeux();
							listGroupes.get(rep).setVoeuIndex(temp, choix);
						}
						
					}
				}

				
				System.out.println("Voulez-vous changer autre chose ? :\n");
				System.out.println("(1) les noms ? \n");
				System.out.println("(2) les voeux ? \n");
				repC = sc.nextInt();
			}
			
			writingGroupesList(taille, listGroupes);
		}
		
		sc.close();
		return;
	}
	
	public static void changerChoixVoeuxGroupe() throws JsonGenerationException, JsonMappingException, IOException {
		System.out.println("Entrez dans modification des choix de sujets\n");
		return;
	}
	
	public static void ajouterGroupe() throws JsonGenerationException, JsonMappingException, IOException {
		System.out.println("Entrez dans ajout groupe\n");
		return;
	}
	
	public static void ajouterSujet() throws JsonGenerationException, JsonMappingException, IOException {
		System.out.println("Entrez dans ajout de sujets\n");
		return;
	}
	
	public static void faireVoeuxPhase2() throws JsonGenerationException, JsonMappingException, IOException {
		System.out.println("Faire voeux phase 2 \n");
		return;
	}
	
	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(System.in);
		System.out.println("Tapez le nombre pour faire votre choix : \n");
		
		System.out.println("(1) : Créer votre gestion de TER de a à z \n");
		System.out.println("(2) : Modifier les sujets \n");
		System.out.println("(3) : Modifier les groupes \n");
		System.out.println("(4) : Ajouter des groupes \n");
		System.out.println("(5) : Ajouter des sujets \n");
		System.out.println("(6) : Faire les voeux pour la phase 2 d'affectations \n");
		System.out.println("() : Sortir \n");
		
		int choix = sc.nextInt();
		
		switch(choix) {
		case 1:
			creationGestion();
			sc.close();
			break;
		case 2:
			modificationSujet();
			sc.close();
			break;
		case 3:
			modificationGroupe();
			sc.close();
			break;
		case 4:
			ajouterGroupe();
			sc.close();
			break;
		case 5:
			ajouterSujet();
			sc.close();
			break;
		case 6:
			faireVoeuxPhase2();
			sc.close();
			break;
		default:
			sc.close();
			return;
		}	
	}

}
